const themeBtn = document.querySelector('.theme-btn');
const page = document.querySelector('.page');
const pageTitle = document.querySelector('.page-title-link-ra');
const navBar = document.querySelector('.nav-bar-box');
const navBarElement = document.querySelectorAll('.nav-bar-element');
const asideBox = document.querySelector('.aside-box');
const asideBoxText = document.querySelectorAll('.aside-box-text');
const textDescription = document.querySelectorAll('.text-description');
const pictureBox = document.querySelector('.picture-box');
const footerBox = document.querySelector('.footer-box');
const mainImage = document.querySelector('.main-image');
if (localStorage.getItem('Theme style') === null){
    localStorage.setItem('Theme style', 'light')
}

changeTheme = () => {
    page.classList.toggle('page-dark');
    pageTitle.classList.toggle('page-title-link-ra-dark');
    navBar.classList.toggle('nav-bar-box-dark');
    for (let elem of navBarElement) {
        elem.classList.toggle('nav-bar-element-dark');
    }
    asideBox.classList.toggle('aside-box-dark');
    for (let elem of asideBoxText){
        elem.classList.toggle('aside-box-text-dark');
    }
    for (let elem of textDescription){
        elem.classList.toggle('text-description-dark');
    }
    pictureBox.classList.toggle('picture-box-dark');
    footerBox.classList.toggle('footer-box-dark');
    mainImage.classList.toggle('main-image-dark');
}
if(localStorage.getItem('Theme style') === 'dark' && !page.classList.contains('page-dark')){
    changeTheme();
}
if (localStorage.getItem('Theme style') === 'light' && page.classList.contains('page-dark')){
    changeTheme();
}

themeBtn.addEventListener('click', () => {
    if (localStorage.getItem('Theme style') === 'light'){
        localStorage.setItem('Theme style','dark');
        changeTheme()
    } else {
        localStorage.setItem('Theme style','light');
        changeTheme()
    }
})